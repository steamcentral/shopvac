# shopvac

An effective, quiet, and economic Cyclone ShopVac to help keep down the dust.

# Goals

## Inexpensive

* uses recycled food grade steel barrell

## Reliable

* use of self-sealing bushings where possible
* few moving parts

## Filterless

* uses cyclonic dust separators

## Large Capacity (1m3 or more for dust storage)

* uses standard steel barrel for dust storage
* 4 inch flow 

## Modular / Expandable

Stacked components are (top to bottom):

* blower (optional, can use shopvac instead)
* secondary small-cylone dust separator (optional)
* primary cyclone dust separator
* bulk dust storage 

## Easily Emptied

* storage barrell slides out for emptying

## Wired for Data Collection

* stubs for adding pressure, flow, and particle sensors

## Easily Monitored

* plexiglass windows for important areas

## Quiet Operation

* effective low RPM blower module
* metallic components wrapped in sound dampening materials 
* use of vibration absorbing mounts where possuble

## Safe

* passive fire suppression for dust storage
* passive automatic mister for dust storage cmpartment
* no heavy parts to lift for emptying

# TODO

* initial specs separating MVP from future wants
* initial design 
* cost and sanity check
* parts design
* initial instructions (build, assembly, installation, usage)
* first build
* update parts spec
* update instructions
* youtube video and web site


# Background Information 

## Cyclone Dust Separator

* [How a Cyclonic Dust Collector Works](https://youtu.be/oZoweO_UX6s)
* [Dyson Cinetic Technology is Amazing](https://youtu.be/O-8Ysa44XrQ)
* [DIY Dust/Chip Collector System - Cheap & Easy](https://youtu.be/5Tz_9HDW3Hg)
* [$25 Dust Collector / Thein Separator](https://youtu.be/-KyZEj8clbs)
* [The American Craftsman Workshop - Dust Collector Update](https://youtu.be/e7kpogfLjRw)

